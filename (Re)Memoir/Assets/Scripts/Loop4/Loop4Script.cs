﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Characters.FirstPerson;
using Transit_Door;
namespace Loop4
{
    public class Loop4Script : MonoBehaviour
    {
        [SerializeField] private Image fade;
        [SerializeField] private Material[] Skyboxes;
        private GameObject TV;
        private bool darkAmbientPlayed = false;
        private GameObject triggerCube;
        private AudioSource music_one;
        private AudioSource music_two;
        [SerializeField] private AudioClip[] thunderSounds;
        private Animator Door;
        private FirstPersonController player;
        private bool enable = false;
        private bool switched = false;
        private Transform firstTarget;
        private Transform secondTarget;
        private Vector3 firstPos;
        private Vector3 secondPos;
        public int state = 0;
        private bool prev = false;
        private GameObject cube;
        public int loopNum = 0;
        bool soundPlay = false;
        private bool darkTrigger = false;
        private float lastThunder;
        private bool transition = false;
        private bool musicFade = false;
        public void MusicFade()
        {
            musicFade = true;
        }
       public void Transition()
        {
            transition = true;
        }
        public void Move()
        {
            if (!prev)
            {
                player.Disable();
                state = 1;
                prev = true;
            }
        }
        public void Trigger()
        {
        }
        public void ResetPrev()
        {
            prev = false;
            loopNum++;
            if (loopNum % 2 == 0)
            {
                secondPos = firstTarget.transform.position;
                firstPos = secondTarget.transform.position;
            }
            else
            {
                firstPos = firstTarget.transform.position;
                secondPos = secondTarget.transform.position;
            }
        }
        private void Start()
        {
            lastThunder = Time.time;
            TV = GameObject.Find("TVMain");
            AudioSource[] sounds;
            sounds = GameObject.Find("FirstPersonCharacter").GetComponents<AudioSource>();
            music_one = sounds[0];
            music_two = sounds[1];
            triggerCube = GameObject.Find("TriggerCube");
            Door = GameObject.Find("Hallway_Door_Loop2").GetComponent<Animator>();
            cube = GameObject.Find("Cube Encasing");
            firstTarget = GameObject.Find("firstTarget").GetComponent<Transform>();
            secondTarget = GameObject.Find("secondTarget").GetComponent<Transform>();
            player = GameObject.Find("Player").GetComponent<FirstPersonController>();
            fade.color = new Color(fade.color.r, fade.color.g, fade.color.b, 1.0f);
            player.Disable();
            firstPos = firstTarget.transform.position;
            secondPos = secondTarget.transform.position;
        }
        private void Update()
        {
            if (musicFade)
            {
                music_two.volume = Mathf.Lerp(music_two.volume, 0.0f, 1.0f * Time.deltaTime);
            }
            else
            {
                if (transition)
                {
                    if (!music_two.isPlaying)
                        music_two.Play();

                    music_two.volume = Mathf.Lerp(music_two.volume, 1.0f, 1.0f * Time.deltaTime);
                    music_one.volume = Mathf.Lerp(music_one.volume, 0.0f, 1.0f * Time.deltaTime);
                }
                if (state == 1)
                {
                    soundPlay = true;
                    player.transform.position = Vector3.Lerp(player.transform.position, firstPos, 0.35f * Time.deltaTime);
                    var lookPos = secondPos - player.transform.position;
                    lookPos.y = 0;
                    var rotation = Quaternion.LookRotation(lookPos);

                    player.transform.rotation = Quaternion.Slerp(player.transform.rotation, rotation, Time.deltaTime * 3.0f);
                }
                else if (state == 2)
                {


                    player.transform.position = Vector3.Lerp(player.transform.position, secondPos, 0.4f * Time.deltaTime);
                    if (soundPlay)
                    {
                        GetComponent<AudioSource>().Play();
                        soundPlay = false;
                    }
                    if (loopNum % 2 == 0)
                        Door.Play("Open");
                    else
                        Door.Play("Open_Reverse");


                }
                if (state == 1 && Vector3.Distance(player.transform.position, firstPos) < 0.5)
                {
                    state = 2;
                }

                if (Vector3.Distance(player.transform.position, secondPos) < 1)
                {
                    cube.SetActive(false);

                    player.Enable();
                    state = 0;
                }
                if (CrossPlatformInputManager.GetButtonDown("Interact"))
                    enable = true;
                if (enable)
                {
                    fade.color = new Color(fade.color.r, fade.color.g, fade.color.b, Mathf.Lerp(fade.color.a, 0.0f, 0.1f));
                    if (fade.color.a <= 0.1f && fade.color.a <= 0.1f && !switched)
                    {
                        player.Enable();
                        switched = true;
                    }

                }
            }
        }

    }
}

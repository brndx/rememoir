﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;
using MainMenu;
using Loop4;
namespace Transit_Door
{
    public class Door : MonoBehaviour
    {
        private FirstPersonController player;
        private MainMenuScript mainmenu;
        private Loop4Script loop4;
        private bool enabled = true;
        private bool first = false;
        void Start()
        {
            
            if (SceneManager.GetActiveScene().name == "Main Menu")
            mainmenu = GameObject.Find("MainMenu").GetComponent<MainMenuScript>();
            else
            loop4 = GameObject.Find("MainMenu").GetComponent<Loop4Script>();
            player = GameObject.Find("Player").GetComponent<FirstPersonController>();
        }
        public void Enable ()
        {
            enabled = true;
        }
        public void Disable()
        {
            enabled = false;
        }
        private void Interact()
        {
            if (enabled)
            {
                first = true;
                if (SceneManager.GetActiveScene().name == "Main Menu")
                {
                    mainmenu.ResetPrev();
                    mainmenu.Move();
                }
                else
                {
                    loop4.ResetPrev();
                    loop4.Move();
                }
                player.Disable();
                enabled = false;
            }
        }
        private void Detach()
        {

        }
        private void Info()
        {
           if (enabled)
              player.DrawIcon();
        }
        void Update()
        {
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PickupObject : MonoBehaviour {
    private FirstPersonController player;

    // Use this for initialization
    void Start () {
        player = GameObject.Find("Player").GetComponent<FirstPersonController>();

    }
    void Info()
    {
        player.DrawIcon();
    }
	// Update is called once per frame
	void Update () {
		
	}
    void Interact()
    {

    }
}

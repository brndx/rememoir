﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MainMenu;
public class PictureFrameTrigger : MonoBehaviour
{
    public GameObject frame1, frame2, frame3;
    private MainMenuScript mm;

    void Start()
    {
        mm = GameObject.Find("MainMenu").GetComponent<MainMenuScript>();
        frame1.GetComponent<Rigidbody>().isKinematic = true;
        frame2.GetComponent<Rigidbody>().isKinematic = true;
        frame3.GetComponent<Rigidbody>().isKinematic = true;

        frame1.GetComponent<Rigidbody>().useGravity = false;
        frame2.GetComponent<Rigidbody>().useGravity = false;
        frame3.GetComponent<Rigidbody>().useGravity = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag=="Player")
        {
            mm.MusicTrigger();
            mm.SoundTrigger();
            frame1.GetComponent<Rigidbody>().isKinematic = false;
            frame2.GetComponent<Rigidbody>().isKinematic = false;
            frame3.GetComponent<Rigidbody>().isKinematic = false;


            frame1.GetComponent<Rigidbody>().useGravity = true;
            frame2.GetComponent<Rigidbody>().useGravity = true;
            frame3.GetComponent<Rigidbody>().useGravity = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag=="Player")
        {
            gameObject.SetActive(false);
        }
    }
}

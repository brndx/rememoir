﻿using UnityEngine;
using System.Collections;
using Transitions;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Characters.FirstPerson;
using NoteManager;
using Loop4;
using MainMenu;
public class Pickup_Trigger : MonoBehaviour
{
    public float lightTvBrightness;
    public Renderer myRenderer;
    public Color defaultTvColor, switchedOffColor;
    public Light myLight;
    public bool switchedOn = true;
    private Loop4Script loop4;
    private MainMenuScript mainmenu;
    private InteractiveTransition it;
    [SerializeField]private int mode = 1;
    private bool carrying;
    private GameObject carriedObject;
    private RawImage noteImg;
    private Text noteText;
    private Note note;
    private float distance = 2;
    private float rayDistance = 3;
    private float smooth = 5;
    private FirstPersonController player;
    private float delayCount = 7.0f;
    private Note notePickup;
    [SerializeField] private Animator animator;
    [SerializeField] private Image whiteFade;
    [SerializeField] private string Fade_Out;
    private bool activateOne = false;
    private bool activateTwo = false;
    //public GameObject lightSwitch;
    
    private void Awake()
    {
    }
    void Start()
    {
        if (SceneManager.GetActiveScene().name == "Main Menu")
        mainmenu = GameObject.Find("MainMenu").GetComponent<MainMenuScript>();
        else
        loop4 = GameObject.Find("MainMenu").GetComponent<Loop4Script>();
        switchedOn = true;
        noteText = GameObject.Find("NoteText").GetComponent<Text>();
        noteImg = GameObject.Find("NoteMain").GetComponent<RawImage>();
        noteImg.color = new Vector4(1, 1, 1, 0);
        player = GameObject.Find("Player").GetComponent<FirstPersonController>();
        it = GameObject.Find("InteractiveTransition").GetComponent<InteractiveTransition>();
    }

    void FixedUpdate()
    {
        if (carrying)
        {
            if (carriedObject.tag == "Pickup")
            {
                carry(carriedObject);
                checkDrop();
            }
        }
        else
        {
                pickup();
        }
    }

    void rotateObject()
    {
        carriedObject.transform.Rotate(5, 10, 15);
    }
    private void Update()
    {
        if (CrossPlatformInputManager.GetButtonDown("Fire2") && noteImg.color.a == 1)
        {
            noteImg.color = new Vector4(1, 1, 1, 0);
            noteText.text = "";
        }
    }
    void carry(GameObject o)
    {
            Rigidbody pickedObject = o.GetComponent<Rigidbody>();
            pickedObject.interpolation = RigidbodyInterpolation.None;
            pickedObject.useGravity = false;
            pickedObject.MovePosition(transform.position + transform.forward);
    }

    void pickup()
    {
        RaycastHit hit;
        if (CrossPlatformInputManager.GetButtonDown("Fire1"))
        {
            // Check if object that can be picked up is present
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, rayDistance))
            {
                if (hit.collider.gameObject.tag == "Note" && hit.collider.gameObject.GetComponent<Renderer>().enabled)
                {
                    hit.collider.gameObject.GetComponent<AudioSource>().Play();
                    note = hit.collider.gameObject.GetComponent<Note>();
                    noteText.text = "To Do : " + '\n' + '\n' + '\n' + note.noteText;
                    noteImg.color = new Vector4(1, 1, 1, 1);
                    hit.collider.gameObject.GetComponent<Renderer>().enabled = false;
                }
                if (hit.collider.gameObject.tag == "Credits")
                {
                    loop4.MusicFade();
                    animator.enabled = true;
                    animator.Play("Fade_Out");
                    StartCoroutine(WhiteFade());
                }
                if (hit.collider.gameObject.tag == "Tv")
                {
                    mainmenu.MusicFade();
                    myRenderer.material.SetColor("_EmissionColor", defaultTvColor);
                    myLight.intensity = lightTvBrightness;
                    animator.enabled = true;
                    animator.Play("Fade_Out");
                    StartCoroutine(WhiteFade());
                }else{
                    myRenderer.material.SetColor("_EmissionColor", switchedOffColor);
                    myLight.intensity = 0;
                }

                if (hit.collider.gameObject.tag == "Pickup" || hit.collider.gameObject.tag == "Sound")
                {
                    if (hit.collider.gameObject.GetComponent<AudioSource>() != null)
                        hit.collider.gameObject.GetComponent<AudioSource>().Play();

                }
                if (hit.collider.gameObject.tag == "Pickup")
                {
                    if (hit.collider.gameObject.name == "robot" && !activateOne)
                    {
                        it.ActivateOne();
                        activateOne = true;
                    }
                    else if (hit.collider.gameObject.name == "toycar1" && !activateTwo)
                    {
                        it.ActivateTwo();
                        activateTwo = false;
                    }

                    carrying = true;
                    carriedObject = hit.collider.gameObject;
                

                    //carriedObject.GetComponent<Rigidbody>().isKinematic = true;

                }

                if(hit.collider.gameObject.name =="Drawer_1" || hit.collider.gameObject.name == "Drawer_2" || hit.collider.gameObject.name == "Drawer_3" || hit.collider.gameObject.name == "Drawer_4")
                {
               
                    Animator anim = hit.collider.gameObject.GetComponent<Animator>();
                    bool isOpen = anim.GetBool("Open");
                    
                    anim.SetBool("Open", !isOpen);
                }

                if (hit.collider.gameObject.name == "switch")
                {
                    Animator anim = hit.collider.gameObject.GetComponent<Animator>();
                    bool isOn = anim.GetBool("On");
                    anim.SetBool("On", !isOn);
             
                }
            }
        }
        else
        {
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, rayDistance))
            {
                if (hit.collider.gameObject.tag == "Sound" || hit.collider.gameObject.tag == "Pickup" || hit.collider.gameObject.tag == "Tv" || hit.collider.gameObject.tag == "Credits" || hit.collider.gameObject.tag == "Note" && hit.collider.gameObject.GetComponent<Renderer>().enabled)
                {
                    player.itemLook = true;
                    player.DrawIcon();
                }
                else
                    player.itemLook = false;
            }
            else
                player.itemLook = false;
        }
    }

    void checkDrop()
    {
        if (!CrossPlatformInputManager.GetButton("Interact"))
        {
            dropObject();
        }
    }
    IEnumerator WhiteFade()
    {
        yield return new WaitForSeconds(delayCount);
        player.itemLook = false;
        if (SceneManager.GetActiveScene().name == "Main Menu")
            SceneManager.LoadScene("Loop4");
        else
            SceneManager.LoadScene("EndGameCredits");
        animator.Play("Fade_In");
    }
    void dropObject()
    {
        carrying = false;
        if (SceneManager.GetActiveScene().name != "Loop4")
        carriedObject.gameObject.GetComponent<Rigidbody>().useGravity = true;
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 mouseDir = mousePos - gameObject.transform.position;
        mouseDir = mouseDir.normalized;
        carriedObject.gameObject.GetComponent<Rigidbody>().AddForce(transform.position + transform.forward * 100.0f);
        // carriedObject.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        carriedObject = null;
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using MainMenu;
public class Door_Transition : MonoBehaviour
{
    private FirstPersonController player;
    private Transform firstTarget;
    private Transform secondTarget;
    private MainMenuScript mainmenu;
    private Animator door;
    private bool next = false;
    private bool enabled = true;
    private bool rotate = false;
    private int state = 0;
    private Vector3 firstPos;
    private Vector3 secondPos;
   private Quaternion rotation = Quaternion.Euler(0, 90.0f, 0);
    void Start()
    {
        mainmenu = GameObject.Find("MainMenu").GetComponent<MainMenuScript>();
        door = GetComponent<Animator>();
        player = GameObject.Find("Player").GetComponent<FirstPersonController>();
    }
    private void Interact()
    {
        if (enabled)
        {
            rotate = true;
            state = 1;
            mainmenu.Move();
            door.Play("Open");
            player.Disable();
            enabled = false;
            if (!next)
                next = true;
        }
    }
    private void Detach()
    {

    }
    private void Info()
    {
        if (enabled)
            player.DrawIcon();
    }
    void Update()
    {
        if (!next)
        {
            firstPos = firstTarget.transform.position;
            secondPos = secondTarget.transform.position;
        }
        else
        {
            secondPos = firstTarget.transform.position;
            firstPos = secondTarget.transform.position;
        }
        if (rotate)
        {
                if (state == 1)
                    player.transform.position = Vector3.Lerp(player.transform.position, firstPos, 0.01f);
                else if (state == 2)
                    player.transform.position = Vector3.Lerp(player.transform.position, secondPos, 0.01f);

                transform.parent.rotation = Quaternion.Slerp(transform.parent.localRotation, rotation, 1.0f * Time.deltaTime);
                if (state == 1 && Vector3.Distance(player.transform.position, firstPos) < 1)
                    state = 2;

                if (Vector3.Distance(player.transform.position, secondPos) < 1)
                {
                    player.Enable();
                    state = 0;
                }
            
        }

    }
}

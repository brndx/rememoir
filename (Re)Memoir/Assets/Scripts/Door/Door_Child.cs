﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Door_Child : MonoBehaviour
{
    private bool active = false;
    private float clampMax;
    private float clampMin;
    private float damp;
    private float currentMouseY;
    private float oldMouseY = 0;
    private float mouseVelocity;
    private float angle;
    private float speed = 5.0f;
    private Transform parent;
    private Quaternion rotation;
    private FirstPersonController player;
    private float lowerLimit = -90.0f;
    private float lastTime = 0;
    private float xDeg;
    private float minSoundSpeed = 1.0f;
    private Quaternion fromRotation;
    private Quaternion toRotation;
    private void Awake()
    {
        GetComponent<AudioSource>().playOnAwake = false;
    }
    private void Start()
    {
        player = GameObject.Find("Player").GetComponent<FirstPersonController>();
        parent = transform.parent;
        
    }
    void Interact(float s)
    {
        active = true;
        speed = s;
    }
    void Detach()
    {
        active = false;
    }
    void Info()
    {
        player.DrawIcon();
    }

    void Update()
    {
        if (active)
        {
            
            xDeg -= Input.GetAxis("Mouse X") * speed;
            fromRotation = transform.parent.rotation;
            xDeg = Mathf.Clamp(xDeg, lowerLimit, 90.0f);
            toRotation = Quaternion.Euler(0, xDeg, 0);
            transform.parent.rotation = Quaternion.Lerp(fromRotation, toRotation, Time.deltaTime * 1.0f);

        }
        else
        {
            if (GetComponent<AudioSource>().isPlaying)
                GetComponent<AudioSource>().Stop();
        }
        if (player.m_usingController)
            minSoundSpeed = 0.1f;
        else
            minSoundSpeed = 1.0f;
        if (Mathf.Abs(Input.GetAxis("Mouse X")) > minSoundSpeed && Time.time - lastTime > 5 && active)
        {

            GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip);
            lastTime = Time.time;
        }


    }

}
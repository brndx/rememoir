﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using MainMenu;

public class CollisionTrigger : MonoBehaviour {
    private FirstPersonController player;
    private MainMenuScript mainmenu;
    [SerializeField] private int mode = 1;
    private void Start()
    {
        mainmenu = GameObject.Find("MainMenu").GetComponent<MainMenuScript>();
        player = GameObject.Find("Player").GetComponent<FirstPersonController>();
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        player.ChangeFootstepSound(mode);
        if (transform.name == "CubeTrigger" && other.name == "Player")
            mainmenu.Trigger();
    }
}

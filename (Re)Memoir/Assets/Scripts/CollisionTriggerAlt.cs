﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Loop4;
public class CollisionTriggerAlt : MonoBehaviour {
    [SerializeField] private int mode;
    [SerializeField] private GameObject Loop1;
    [SerializeField] private GameObject Loop2;
    [SerializeField] private GameObject Loop3;
    [SerializeField] private GameObject Loop4;
    [SerializeField] private GameObject Loop5;
    [SerializeField] private GameObject Loop6;
    private bool active = false;
    private Loop4Script loop4;
    private GameObject[] Loops;
    void Start () {
        loop4 = GameObject.Find("MainMenu").GetComponent<Loop4Script>();
        GameObject[] temp = { Loop1, Loop2, Loop3, Loop4, Loop5, Loop6 };
        Loops = temp;
        for (int i = 2; i < Loops.Length; i++)
            Loops[i].SetActive(false);
    }

    public void OnTriggerEnter(Collider other)
    { 

        if (other.name == "Player")
        {
            if (mode == 5)
            {
                if (!active)
                {
                    loop4.Transition();
                    active = true;
                }

            }
            for (int i = 0; i < Loops.Length; i++)
            {
                if (i > mode || i < mode - 2)
                    Loops[i].SetActive(false);
            }
            if (mode > 1)
                Loops[mode - 2].SetActive(true);
            if (mode < 6)
                Loops[mode].SetActive(true);
        }
    }

}

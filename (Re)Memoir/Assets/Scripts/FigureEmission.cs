﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace DarkFigure {
    public class FigureEmission : MonoBehaviour
    {
        public Material emissiveMat;
        public GameObject darkFigure;
        private bool startFade = false;
        public void MusicFade()
        {
            if (!startFade)
                startFade = true;
        }
        void Awake()
        {
            emissiveMat.DisableKeyword("_EMISSION");

        }
        private void Update()
        {
            if (startFade)
            GetComponent<AudioSource>().volume = Mathf.Lerp(GetComponent<AudioSource>().volume, 0.0f, 1.0f * Time.deltaTime);

        }
        void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                StartCoroutine(DarkFigureFade());
            }


        }
        IEnumerator DarkFigureFade()
        {
            yield return new WaitForSeconds(1f);
            darkFigure.SetActive(false);
            emissiveMat.EnableKeyword("_EMISSION");
            GetComponent<AudioSource>().Play();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTextureSetup : MonoBehaviour
{
    [SerializeField] private Camera cameraB;
    [SerializeField] private Camera cameraA;
    [SerializeField] private Camera cameraC;
    [SerializeField] private Camera cameraD;
    [SerializeField] private Camera cameraE;
    [SerializeField] private Camera cameraF;
    [SerializeField] private Camera cameraG;
    [SerializeField] private Camera cameraH;
    [SerializeField] private Camera cameraI;
    [SerializeField] private Camera cameraJ;
    [SerializeField] private Material cameraMatB;
    [SerializeField] private Material cameraMatA;
    [SerializeField] private Material cameraMatC;
    [SerializeField] private Material cameraMatD;
    [SerializeField] private Material cameraMatE;
    [SerializeField] private Material cameraMatF;
    [SerializeField] private Material cameraMatG;
    [SerializeField] private Material cameraMatH;
    [SerializeField] private Material cameraMatI;
    [SerializeField] private Material cameraMatJ;

    void Start () 
    {
        Camera[] cameras = { cameraA, cameraB, cameraC, cameraD, cameraE, cameraF, cameraG, cameraH, cameraI, cameraJ };
        Material[] materials = { cameraMatA, cameraMatB, cameraMatC, cameraMatD, cameraMatE, cameraMatF, cameraMatG, cameraMatH, cameraMatI, cameraMatJ };
        for (int i = 0; i < cameras.Length; i++)
        {
            // Initialise render to texture materials and their associated cameras
            if (cameras[i].targetTexture != null)
            {
                cameras[i].targetTexture.Release();
                cameras[i].targetTexture = new RenderTexture(Screen.width, Screen.height, 24);
                materials[i].mainTexture = cameras[i].targetTexture;
            }
        }
        

    }


}

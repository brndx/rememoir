﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MainMenu;
public class FlickeringLight : MonoBehaviour
{
    
    Light testLight; 
    private MainMenuScript mms;
    private float limit = 0.9f;
    private bool start = false;


	void Start ()
    {
        mms = GameObject.Find("MainMenu").GetComponent<MainMenuScript>();
        testLight = GetComponent<Light>();
	}

    private void Update()
    {
       
        if (mms.loopNum > 1 && mms.state == 2)
        {
            start = true;
        }
        if (start)
        {
            if (Random.value > limit)
                testLight.enabled = !testLight.enabled;
        }
    }


}
